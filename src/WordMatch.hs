{-# LANGUAGE OverloadedStrings #-}

module WordMatch where

import Prelude hiding ()
import Data.List
import Data.Map as M hiding (map)
import qualified Data.ByteString as B
import qualified Data.Char as C
import qualified Data.Text as T
import qualified Data.Text.Encoding as EN
import qualified Data.Text.IO as TI

filename :: String
filename = "wordsEn.txt"

-- Counts the number of words per line
countWordLength :: T.Text -> [Int]
countWordLength input = map (T.length) (take 10 $ T.lines input)

countLinie :: T.Text -> Int
countLinie input = length (T.lines input)

prepareLine :: T.Text -> T.Text
prepareLine line = T.init line

sortLetters :: T.Text -> T.Text
sortLetters  = T.pack . sort . T.unpack

wordToPair :: T.Text -> (T.Text, T.Text)
wordToPair word = (sortLetters word, word)

sortAndGroup :: [(T.Text, T.Text)] -> Map T.Text [T.Text]
sortAndGroup assocsList = fromListWith (++) [(k, [v]) | (k, v) <- assocsList]

list2Map :: [T.Text] -> Map T.Text [T.Text]
list2Map = sortAndGroup . map wordToPair

offset :: Int
offset = C.ord 'a' - 1

letter2number :: Char -> Int
letter2number c = C.ord c - offset

plus10 t = 10 + t

readDictionary :: IO (Map T.Text [T.Text])
readDictionary = do
  input <- TI.readFile filename
  let inputLines = T.lines input
  let inputWords = map prepareLine inputLines
  let wordMap = list2Map inputWords
  return wordMap

--List should just be a text
unscrambleText :: (Map T.Text [T.Text]) -> T.Text -> T.Text
unscrambleText dict currentWord = resultText
    where 
        sortedWord = sortLetters currentWord
        resultList = M.lookup sortedWord dict
        resultText = case resultList of
            Just words -> T.unlines words
            Nothing -> "" :: T.Text

unscrambleByteString :: (Map T.Text [T.Text]) -> B.ByteString -> B.ByteString
unscrambleByteString dict bytestring = EN.encodeUtf8 $ unscrambledText
   where
       text = EN.decodeUtf8 bytestring
       unscrambledText = unscrambleText dict text
       