{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Applicative
import           Data.List
import           Data.Map hiding (map)
import qualified Data.Text as T
import qualified Data.ByteString as B
import           Snap.Core
import           Snap.Util.FileServe
import           Snap.Http.Server

import WordMatch

main :: IO ()
main = do
    wordMap <- readDictionary
    quickHttpServe $ site wordMap

site :: (Map T.Text [T.Text]) -> Snap ()
site wordMap =
    ifTop (writeBS "hello world") <|>
    route [ ("foo", writeBS "bar")
          , ("echo/:echoparam", echoHandler)
          , ("word/:echoparam", wordHandler wordMap)
          ] <|>
    dir "static" (serveDirectory ".")

echoHandler :: Snap ()
echoHandler = do
    param <- getParam "echoparam"
    maybe (writeBS "must specify echo/param in URL")
          writeBS param

wordHandler :: (Map T.Text [T.Text]) -> Snap ()
wordHandler wordMap = do
    param <- getParam "echoparam"
    maybe (writeBS "must specify echo/param in URL")
          writeBS $ fmap (unscrambleByteString wordMap) param
