{-# LANGUAGE OverloadedStrings #-}

module WordMatchSpec where
import Prelude hiding ()
import           WordMatch

import qualified Data.Map   as M
import           Test.Hspec
import Data.Char
import Data.Text as T hiding (map, toUpper)

spec :: Spec
spec = do
  describe "sortLetters" $ do
    it "string without tab" $
      "aims" `shouldBe` sortLetters "sami"
    it "empty string" $
      "" `shouldBe` sortLetters ""

  describe "prepareLine" $ do
    it "string without tab" $
      "sami" `shouldBe` prepareLine "sami\n"

  describe "wordToPair" $ do
    it "string" $
      ("aims", "sami") `shouldBe` wordToPair "sami"
    it "string list" $
      [("aims", "sami")] `shouldBe` map wordToPair ["sami"]

  describe "list2Map" $ do
    it "string" $
      M.fromList [("aims",["sami"])] `shouldBe` list2Map ["sami"]

  describe "Map lookup" $ do
    it "string" $
      Just ["sami"] `shouldBe` M.lookup "aims" (M.fromList [("aims",["sami"])])

  describe "toUpper" $ do
    it "letter f" $
      toUpper 'f' `shouldBe` 'F'
    it "frejA" $
      map toUpper "frejA" `shouldBe` "FREJA"

  describe "plus10" $ do
    it "plus10 1" $
      plus10 1 `shouldBe` 11
    it "plus10 1o" $
      plus10 10 `shouldBe` 20
    it "plus10 20" $
      plus10 20 `shouldBe` 30
    it "plus10 [1, 10, 20]" $
      map plus10 [1, 10, 20] `shouldBe` [11, 20, 30]
    
  describe "letter2number" $ do
    it "letter2number 'a' = 1" $
      letter2number 'a' `shouldBe` 1
    it "letter2number 'b' = 2" $
      letter2number 'b' `shouldBe` 2
    it "map letter2number brunch" $
      map letter2number "brunch" `shouldBe` [2,18,21,14,3,8]
    it "map letter2number dragon" $
      map letter2number "dragon" `shouldBe` [4,18,1,7,15,14]
    it "map letter2number triangle" $
      map letter2number "triangle" `shouldBe` [20,18,9,1,14,7,12,5]
      
--freja b
      --fwbbwffwbbwf          qut bgtyhnujmiiiikolp!!!!!!!
      
      
      